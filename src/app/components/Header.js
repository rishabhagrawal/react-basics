import React from 'react';

export class Header extends React.Component {
	render(){
		return (
			<nav className="navbar navbar-default">
				<div className="container">
					<ul>
						<li>
							<a href="#">Home</a>
						</li>
						<li>
							<a href="#">Company</a>
						</li>
					</ul>
				</div>
			</nav>
		);
	}
}