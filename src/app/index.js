import React from 'react';
import { render } from 'react-dom';

import { Header } from './components/Header';
import { Home } from './components/Home';

class App extends React.Component {
    render() {
        return (
        	<div>
        		<Header/>
        		<h1>Hello Rishabh!</h1>
        		<Home/>
        		<h2>Hello React</h2>
        	</div>
        );
    }
}

render(<App/>, window.document.getElementById('app'));
