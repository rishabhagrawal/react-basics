import React from 'react';

export class Home extends React.Component {
    render() {
    	let content = "";
    	if(true){
    		content = <p>Hello Content</p>;
    	}
        return (
        	<div>
        		<p>In a new Component</p>
        		<p>{ 9 + 8 }</p>
        		{content}
        		{"Hello String"}
        		<p>
        		{ 5 == 2 ? "yes" : "No"}
        		</p>
        	</div>
        );
    }

}
